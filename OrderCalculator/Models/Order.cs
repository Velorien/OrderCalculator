﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCalculator.Models
{
    public class Order
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Imię jest wymagane.")]
        [Display(Name = "Kto żre?")]
        [StringLength(30, ErrorMessage = "Imię musi mieć od 1 do 30 znaków.", MinimumLength = 1)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Wartość paszy jest wymagana.")]
        [Display(Name = "Wartość paszy")]
        public double OrderValue { get; set; }

        [Required(ErrorMessage = "Wkład własny jest wymagany (nawet jeśli nie masz kasy).")]
        [Display(Name = "Wpłata własna")]
        public double Input { get; set; }
    }
}
