﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCalculator.Models
{
    public class Transfer
    {
        public Transfer()
        { }

        public Transfer(string to, string from, double value)
        {
            To = to;
            From = from;
            Value = value;
        }

        public string To { get; set; }

        public string From { get; set; }

        public double Value { get; set; }
    }
}
